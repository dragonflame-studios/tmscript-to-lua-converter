﻿//TMScript To Lua Conversion Library
//This library handles writing to the application's console textbox, as well

using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.IO;

using TMS2LTranslationLib;

namespace TMS2LConversionLib
{
    public class ConsoleHandler
    {
        public static void ConsoleWriteLine(TextBox textBox, string msg, int msgType = 0 ,bool newLine = true)
        {
            //msgType: 0 - Log, 1 - Notice, 2 - Warning, 3 - Error
            //Default is Type 0 - Log

            switch (msgType)
            {
                case 0:
                    msg = "LOG: " + msg;
                    break;
                case 1:
                    msg = "NOTICE: " + msg;
                    break;
                case 2:
                    msg = "WARNING: " + msg;
                    break;
                case 3:
                    msg = "ERROR: " + msg;
                    break;
                default:
                    msg = "Log: " + msg;
                    break;
            }
            //Append 24h Time to start of console message.
            DateTime curDate = DateTime.Now;
            string formattedDate = curDate.ToString("HH:mm:ss");
            msg = formattedDate + " - " + msg;

            //FUTURE THING: Apply special formatting for warnings and errors. The color of a warning will be a yellow, and an error will be red.
            //This will require switching from TextBox to RichTextBox.


            //Append a NewLine character if newLine is true, which it is by default.
            if (newLine == true)
            {
                msg += "\n";
                textBox.Text += msg;
            }
            else
            {
                textBox.Text += msg;
            }
        }
    }
    

    public class ConversionLib
    {
        public static Tuple<int, string> ConvertScript(string TMScriptFilePath, string LuaScriptFilePath, TextBox consoleWindow)
        {
            //This method will be entirely responsible for doing the line-by-line interpretation and transcompilation/translation from TMScript to Lua.
            //Primary way it will perform this, for now, is reading the TMScript file Line-By-Line, splitting the command from its parameters, and running the command against a translation dictionary class.
            //If the dictionary contains the command then in the output Lua file the effective translation is written with parameters in their correct spots.
            //If the dictionary does NOT contain the command, then it stops the translation and logs a missing translation error to the window's console.
            ConsoleHandler.ConsoleWriteLine(consoleWindow, "Successfully jumped into start of Conversion Loop");

            try
            {
                string oldCmd = "test";
                string newCmd = TMS2LTranslationLib.TMSTranslator.CommandTranslate(oldCmd);
                ConsoleHandler.ConsoleWriteLine(consoleWindow, "Command Translated: " + oldCmd + " -> " + newCmd);
            }
            catch(KeyNotFoundException keyErr)
            {
                ConsoleHandler.ConsoleWriteLine(consoleWindow, keyErr.Message, 3);
            }

            var returnCode = 0;
            var returnMsg = "";
            return Tuple.Create(returnCode, returnMsg);
        }
    }
}