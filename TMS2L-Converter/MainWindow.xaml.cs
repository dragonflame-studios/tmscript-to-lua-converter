﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms;

using System.IO;

using TMS2LConversionLib;

namespace TMS2L_Converter
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            TMS2L_Console_Output.Clear();
        }

        private void ClearConsole_Button_Click(object sender, RoutedEventArgs e)
        {
            //Lol just clear the textbox, ez
            TMS2L_Console_Output.Clear();
        }

        private void ConvertScript_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //Disable this message box since technically its being implemented, and instead post notice to console log.
                //System.Windows.Forms.MessageBox.Show("This is not implemented yet.", "Not Implemented");
                ConsoleHandler.ConsoleWriteLine(TMS2L_Console_Output, "Conversion is not completely implemented yet. Expect errors and mistranslations.", 1);

                //We get and check the filepaths set in the window's path textboxes, and check to see if its valid selections before even remotely attempting to begin conversions.
                FileInfo TMScriptFileInfo = new FileInfo(TMScript_File_Path.Text);
                DirectoryInfo LuaScriptFileInfo = new DirectoryInfo(Lua_File_Path.Text);

                bool TMScriptPathIsValid = TMScriptFileInfo.Exists;
                bool LuaScriptPathIsValid = LuaScriptFileInfo.Exists;

                if (TMScriptPathIsValid && LuaScriptPathIsValid)
                {
                    ConsoleHandler.ConsoleWriteLine(TMS2L_Console_Output, "Validated that both paths are valid");
                    ConversionLib.ConvertScript(TMScript_File_Path.Text, Lua_File_Path.Text, TMS2L_Console_Output);
                }
                else
                {
                    ConsoleHandler.ConsoleWriteLine(TMS2L_Console_Output, "One or both paths are invalid", 3);
                }

            }
            catch (System.ArgumentException)
            {
                ConsoleHandler.ConsoleWriteLine(TMS2L_Console_Output, "An invalid or missing file path was detected. Please check your path and names.", 3);
            }
            catch (Exception err)
            {
                ConsoleHandler.ConsoleWriteLine(TMS2L_Console_Output, err.Message, 3);
            }
        }
        private void Lua_File_Path_Browse_Click(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            DialogResult result = fbd.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath)) 
            { 
                Lua_File_Path.Text = fbd.SelectedPath;
                ConsoleHandler.ConsoleWriteLine(TMS2L_Console_Output, "Primed Lua File Path", 0);
            }
        }

        private void TMScript_File_Path_Browse_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog openFileDialog = new Microsoft.Win32.OpenFileDialog();
            openFileDialog.Filter = "TMScript Files (*.tmscript)|*.tmscript";
            if (openFileDialog.ShowDialog() == true)
            {
                TMScript_File_Path.Text = openFileDialog.FileName;
                ConsoleHandler.ConsoleWriteLine(TMS2L_Console_Output, "Primed TMScript File Path", 0);
            }
        }
    }

}

