﻿using System.Collections.Generic;

namespace TMS2LTranslationLib
{
    public class TMSTranslator
    {
        public static string CommandTranslate(string command)
        {
            string[] translatedCommand;
            if (!commandDictionary.ContainsKey(command))
            {
                throw new KeyNotFoundException("Unknown Command: "+ command);
            }
            else
            {
                translatedCommand = commandDictionary[command];
                return translatedCommand[0];
            }
            
        }

        public static Dictionary<string, string[]> commandDictionary = new Dictionary<string, string[]>
        {
            { "test", new string[] {"OOPS"} },
            { "foo", new string[] {"bar" } }

        };
    }
}